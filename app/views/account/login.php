<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Login</title>
    <link href="<?php echo URL::to('/');?>/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
   <!--  http://localhost/laravel/public/bootstrap/css/bootstrap.min.css -->
    <link href="<?php echo URL::to('/');?>/css/style.css" rel="stylesheet" type="text/css" />
</head>
<body id="login-page">
    <div id="login" class="container">
        <div class="row logo">
            <div class="span4 offset4 center">
                <?php if (!empty($notif)): ?>
                    <div class="alert alert-error" style="width:240px; margin-top:-48px;">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Oopsss!</strong> <?php echo $notif; ?>
                    </div>
                <?php endif ?>
                <a href="<?php echo URL::current(); ?>">
                    <h3>Lagu Karo</h3>
                    <!-- <img src="<?php echo URL::current(); ?>public/app/img/logo.png" height="55" alt="logo" /> -->
                </a>
            </div>
        </div>
        <div class="row">
            <div class="span4 offset4">
                <?php echo Form::open(array('id'=>"login",'class'=>'well','method'=>'POST','url'=>'account/proses_login')); ?><!-- <form id="login" class="well" method="POST" action="<?php echo URL::current() ?>login"> -->
                <div class="control-group">
                    <div class="input-prepend">
                    <span class="add-on"><i class="icon-user"></i></span>
                        <?php echo form::text('username','',array('class' => 'awesome','id'=>'inputUsername','placeholder'=>'Emailndu')); ?>
                        <!--<input id="inputUsername" type="text" placeholder="Emailndu" name="username">-->
                    </div>
                </div> 
                <div class="control-group">
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-lock"></i></span>
                        <?php echo form::password('password',array('id'=>'inputPassword', 'placeholder'=>'Kata sandindu')) 
                        ?>
                        <!-- <input id="inputPassword" type="password" placeholder="Kata sandindu" name="password" -->
                    </div>
                </div>
               <?php //echo Form::submit('Log Isn',array('class'=>'btn btn-large btn-block btn-primary')); ?> <!-- tidak di buat karena ada perubahan tampilan -->
                 <button type="submit" class="btn btn-large btn-block btn-primary">Log In</button> 
                <?php echo Form::close(); ?>
            </div>
        </div>
    </div>

    <!-- JS -->
    <script src="<?php echo URL::to('/'); ?>/jquery/js/jquery-1.7.2.min.js"></script>
    <script src="<?php echo URL::to('/'); ?>/bootstrap/js/bootstrap-alert.js"></script>
    
</body>
</html>