@extends('templates.default')

@if($errors->has())
<ul>
	<p>{{ $errors->first('nama', '<li>:message</li>')}}</p>
</ul>
@endif

	@section('content')
	<h3> Add Account </h3>
	{{ Form::open(array('url' => 'account/proseslogin','method'=>'GET')) }}
		<p>
			{{ Form::label('nama','Nama: ') }}
			{{ Form::text('nama') }}
		</p>

		<p>
			{{ Form::label('email','Email: ') }}
			{{ Form::text('email') }}
		</p>

		<p>
			{{ Form::label('password','Password: ') }}
			{{ Form::password('password') }}
		</p>

		<p>{{ Form::submit('Input') }}
	{{Form::close()}}
@endsection
 