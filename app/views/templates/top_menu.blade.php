	<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="navbar-inner">
		<div class="container">
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			<a class="brand" href="#">Laravel 4 + Bootsraap</a>
		    <ul class="nav ">
			    <li class="active">{{ HTML::link('/','Home')}}</li>
			    <li>{{ HTML::link('#','Link')}}</li>
			    <li>{{ HTML::link('#','Link')}}</li>
			    <li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#test1">
						Dropdown test
						<b class="caret"></b>
					</a>
					<ul class="dropdown-menu">
						<li><a href="#test2">aa</a> </li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</nav>


<script src="<?php echo URL::to('/');?>/jquery/js/jquery.js"></script>
<script src="<?php echo URL::to('/');?>/bootstrap/js/bootstrap.js"></script>
<script src="<?php echo URL::to('/');?>/bootstrap/js/bootstrap-dropdown.js"></script>
<link href="<?php echo URL::to('/');?>/bootstrap/css/bootstrap.css" rel="stylesheet">
<link href="<?php echo URL::to('/');?>/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
<link href="<?php echo URL::to('/');?>/bootstrap/js/bootstrap-dropdown.js" rel="stylesheet">
<link href="<?php echo URL::to('/');?>/bootstrap/css/editcss.css" rel="stylesheet">

<script>
$('.dropdown-toggle').dropdown();

</script>

<div class="hero-unit" >