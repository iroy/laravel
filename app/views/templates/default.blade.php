<!DOCTYPE html>
<html>
<head>
	<title>{{$title}}</title>
<body>
	@include('templates.top_menu')
	@if(Session::has('pesan'))
	<p style="color: green">{{Session::get('pesan')}}</p>
	@endif

	@yield('content')
</body>
</html>