<?php

//Route::controller('lagu','Lagu_Controller');
Route::controller('account','Account_Controller'); //me-route controller account yg ada di application/controllers/account.php

Route::get('/', function()// ke lokasi laravel/app/
{
	return Redirect::to('account/viewlagu')->with('message', 'Selamat Datang');
	//return Route::make('account.profile');// buka view/account/profile 'dpt diganti account/profile'
});

Route::get('account/editlagu/{id}',array('as'=>'edit_lagu','uses'=>'account@editlagu'));
Route::post('account/updatelagu/',array('as'=>'updatelagu','uses'=>'account@updatelagu'));
Route::post('account/hapuslagu/{id}',array('as'=>'hapuslagu','uses'=>'account@hapuslagu'));

 //Route::controller('account',array('as'=>'account','uses'=>'account@view'));

// Route::get('user', array('as' => 'User','uses'=>'User@index'));

// Route::filter('before',function()
// {

// 	return 'hasil dari filter before';
// });

// Route::filter('account', function() 
// {
//     if (Input::get('name') < 200)
//     {
//         return Redirect::to('account/login');
        
//     }
// });

// Route::get('account/profile', array('as' => 'profile', 'do' => function()
// {
// return View::make('account/profile');
// }));

//application/routes.php

// Route::get('users', function() // ke lokasi laravel/app/users
// {

//     return View::make('account.profile');
//     // return 'Users!, anda berada di ';
//     // return  URL::base();
//     //echo "alamat : ";
//     //HTML::mailto('me@daylerees.com', 'Mail me!');// <a href="mailto:me@daylerees.com">Mail me!</a>
//     //echo HTML::link('my/page', 'My Page'); // pindah ke alamat base + my/page dengan tampilan My Page
//     // echo URL::full(); // mengambil seluruh alamat url / 'echo untuk menampilkan'
//     // echo URL::to('account/login'); // mengambil alamat dari base url ditambah 'account/login'
//     // echo URL::to_route('logout');
// });