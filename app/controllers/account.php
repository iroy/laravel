<?php

class Account_Controller extends BaseController
{
    
    public function getIndex()
    {
        echo "This is the profile page. di accountcontroler";
    }
    public function getLogin()
    {
        //return URL::route('account@logout');
    }
    public function getLogout()
    {
        echo "This is the logout action.";
    }

    public function getNew()
    {
        return View :: make ('account.new')
            ->with('title','Tambah Account');
    }

    public function getProseslogin(){
        $validasi=User::validasi(Input::all());

        if ($validasi->fails()) {
            return Redirect::to('account/new')->withErrors($validasi)->withInput();
        }else{
            User::create(array(
            'nama'=>Input::get('nama'),
            'email'=>Input::get('email'),
            'password'=>Input::get('password')
            ));
        return Redirect::to('account/prosesnew')->with('message', 'Account baru telah dibuat');
        }
    }

    public function getProsesnew(){
        return View :: make ('account.form')
            ->with('title','title dari account')
            ->with('data',User::orderBy('nama')->get());
    }
 
    public function getProfile(){
        echo "This is the profile action. <h1>echo di application/controllers/account.php";
    }

    public function getView(){
        return View :: make ('account.form')
            ->with('title','title dari account')
           ->with('data',User::orderBy('id')->get());
    }

    public function getViewlagu(){
        return View :: make ('lagu.list')
            ->with('title','list lagu')
           ->with('data',Lagu::orderBy('id')->get());
    }

    public function getWelcome($name,$place){
        $data=array('name'=> $_GET['name'],
                    'place'=>$_GET['place'] );
	   return View :: make('welcome',$data);
	}

    public function getEditlagu($id){
        return View::make('lagu.edit')
            ->with('title','edit lagu')
            ->with('data',Lagu::find($id));
    }

    public function getUpdatelagu(){
        $user=Lagu::find(Input::get('id'));
        $user->judul=Input::get('judul');
        $user->lokasi=Input::get('lokasi');
        $user->penyanyi=Input::get('penyanyi');
        $user->save();
        return Redirect::to('account/viewlagu')->with('message', 'Account telah di edit');
    }

    public function getHapuslagu($id){
        Lagu::destroy($id);
        return Redirect::to('account/viewlagu')->with('message', 'Account telah di hapus');
    }
}


